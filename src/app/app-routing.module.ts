import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {ModalComponent} from './components/modal/modal.component';
import {CarouselComponent} from './components/carousel/carousel.component';
import {TabsComponent} from './components/tabs/tabs.component';


const routes: Routes = [
  {path: '', redirectTo: 'modal', pathMatch: 'full'},
  {path: 'modal', component: ModalComponent },
  {path: 'carousel', component: CarouselComponent },
  {path: 'tabs', component: TabsComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
